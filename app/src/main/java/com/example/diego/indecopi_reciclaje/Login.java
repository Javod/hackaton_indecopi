package com.example.diego.indecopi_reciclaje;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Login extends AppCompatActivity {

    private Button btnIngresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initComponents();
        ingresar();
    }

    public void initComponents()
    {
        btnIngresar = findViewById(R.id.btn_ingresar_principal);
    }

    public void ingresar()
    {
        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(Login.this, Principal.class));
            }
        });
    }
}
