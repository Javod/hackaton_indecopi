package com.example.diego.indecopi_reciclaje;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.example.diego.indecopi_reciclaje.beans.Envio;
import com.example.diego.indecopi_reciclaje.conexion.rest.RestClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private Marker marcador;
    private double lat = 0;
    private double lon = 0;
    private List<Envio> envios;
    private List<LatLng> coordenadas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        envios = new ArrayList<>();
        coordenadas = new ArrayList<>();
        obtenerEnviosRest();

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;



       miUbicacion();
    }

    public void agregarMarcador(double lat, double lon) {
        LatLng cordenada = new LatLng(lat, lon);
        CameraUpdate miUbicacion = CameraUpdateFactory.newLatLngZoom(cordenada, 16);

        marcador = mMap.addMarker(new MarkerOptions()
                .position(cordenada)
                .title("Mi Posicion")
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher_foreground)));
        mMap.animateCamera(miUbicacion);

    }

    public void actualizarUbicaion(Location location) {
        if (location != null) {
            lat = location.getLatitude();
            lon = location.getLongitude();
            agregarMarcador(lat, lon);
        }
    }

    LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            actualizarUbicaion(location);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    public void obtenerEnviosRest()
    {
        String url = "http://192.168.113.132:8080/ServicioRestIndecopyReciurban/rest.envio/";
        Retrofit retrofit = new Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build();
        RestClient restCliente = retrofit.create(RestClient.class);

        Call<List<Envio>> callEnvio= restCliente.getEnvios();

        callEnvio.enqueue(new Callback<List<Envio>>() {
            @Override
            public void onResponse(Call<List<Envio>> call, Response<List<Envio>> response) {
                envios = response.body();

                if(envios!=null)
                {
                    for (Envio e : envios)
                    {
                        agregarMarcador(e.getLatitud(),e.getLongitud());
                    }
                }


                Toast.makeText(MapsActivity.this, "Se conecto correctamente", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<List<Envio>> call, Throwable t) {

                Toast.makeText(MapsActivity.this, "No conecto", Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void miUbicacion() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationManager locationManager = (LocationManager) getSystemService((Context.LOCATION_SERVICE));
        Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        actualizarUbicaion(location);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,15000,0,locationListener);

    }
}
