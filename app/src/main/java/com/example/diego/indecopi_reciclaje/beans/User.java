package com.example.diego.indecopi_reciclaje.beans;

import java.io.Serializable;

public class User  implements Serializable {

    private int idUser;
    private String nombreUser;
    private String dni;
    private String correo;
    private String contrasena;
    private String dirrecion;
    private int rankingPuntaje;

    public User()
    {

    }

    public User(int idUser, String nombreUser, String dni, String correo, String contrasena, String dirrecion, int rankingPuntaje) {
        this.idUser = idUser;
        this.nombreUser = nombreUser;
        this.dni = dni;
        this.correo = correo;
        this.contrasena = contrasena;
        this.dirrecion = dirrecion;
        this.rankingPuntaje = rankingPuntaje;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getNombreUser() {
        return nombreUser;
    }

    public void setNombreUser(String nombreUser) {
        this.nombreUser = nombreUser;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getDirrecion() {
        return dirrecion;
    }

    public void setDirrecion(String dirrecion) {
        this.dirrecion = dirrecion;
    }

    public int getRankingPuntaje() {
        return rankingPuntaje;
    }

    public void setRankingPuntaje(int rankingPuntaje) {
        this.rankingPuntaje = rankingPuntaje;
    }
}
