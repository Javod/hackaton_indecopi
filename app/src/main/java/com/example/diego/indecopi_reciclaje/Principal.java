package com.example.diego.indecopi_reciclaje;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.diego.indecopi_reciclaje.beans.Envio;
import com.example.diego.indecopi_reciclaje.conexion.rest.RestClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Principal extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private LinearLayout btnPapel;
    private LinearLayout btnPlastico;
    private LinearLayout btnVidrio;
    private LinearLayout btnmetal;
    private LinearLayout btnElectro;
    private Envio envio;
    boolean bandera = false;
    boolean bandera1 = false;
    boolean bandera2 = false;
    boolean bandera3 = false;
    boolean bandera4 = false;
    boolean validabandera = false;
    private LocationManager locManager;
    private Location loc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initComponets();
        miUbicacion();
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (validabandera == false) {
                    mensaje("Escoger un tipo de reciclaje");
                    return;
                }


                mensajeDialoj();
            }
        });



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        btnPapel.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {


                if (bandera == false) {
                    btnPapel.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.tab_btn_cambio));
                    envio.setPtVidrio(1);
                    bandera = true;
                    validabandera = true;
                } else if (bandera) {
                    btnPapel.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.fondo_tacho));
                    envio.setPtVidrio(0);
                    bandera = false;
                    validabandera = false;
                }
            }
        });

        btnElectro.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {

                if (bandera1 == false) {
                    btnElectro.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.tab_btn_cambio));
                    bandera1 = true;
                    envio.setPtElectro(4);
                    validabandera = true;
                } else if (bandera1) {
                    btnElectro.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.fondo_tacho));
                    bandera1 = false;
                    envio.setPtElectro(0);
                    validabandera = false;
                }

            }
        });

        btnVidrio.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {


                if (bandera2 == false) {
                    btnVidrio.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.tab_btn_cambio));
                    envio.setPtVidrio(1);
                    bandera2 = true;
                    validabandera = true;
                } else if (bandera2) {
                    btnVidrio.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.fondo_tacho));
                    envio.setPtVidrio(0);
                    bandera2 = false;
                    validabandera = false;
                }
            }
        });

        btnPlastico.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {


                if (bandera3 == false) {
                    btnPlastico.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.tab_btn_cambio));
                    envio.setPtVidrio(1);
                    bandera3 = true;
                    validabandera = true;
                } else if (bandera3) {
                    btnPlastico.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.fondo_tacho));
                    envio.setPtVidrio(0);
                    bandera3 = false;
                    validabandera = false;
                }
            }
        });

        btnmetal.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {


                if (bandera4 == false) {
                    btnmetal.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.tab_btn_cambio));
                    envio.setPtVidrio(1);
                    bandera4 = true;
                    validabandera = true;
                } else if (bandera4) {
                    btnmetal.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.fondo_tacho));
                    envio.setPtVidrio(0);
                    bandera4 = false;
                    validabandera = false;
                }
            }
        });


    }
    public void actualizarUbicaion(Location location) {
        if (location != null) {
            envio.setLatitud(location.getLatitude());
            envio.setLongitud(location.getLongitude());

        }
    }

    LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            actualizarUbicaion(location);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    public void enviarDatosRest(Envio envio)
    {
        String url = "http://192.168.113.132:8080/ServicioRestIndecopyReciurban/rest.envio/";
        Retrofit retrofit = new Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build();
        RestClient restCliente = retrofit.create(RestClient.class);
        Call<Envio> enviarDatos = restCliente.enviarPedido(envio);

        enviarDatos.enqueue(new Callback<Envio>() {
            @Override
            public void onResponse(Call<Envio> call, Response<Envio> response) {

                if(response.isSuccessful())
                {
                    Toast.makeText(Principal.this, "Conectado", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<Envio> call, Throwable t) {

                mensaje("no se establecio la conexion");

            }
        });


    }


    private void miUbicacion() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationManager locationManager = (LocationManager) getSystemService((Context.LOCATION_SERVICE));
        Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        actualizarUbicaion(location);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,15000,0,locationListener);

    }

    public void initComponets() {
        btnPapel = findViewById(R.id.btn_papel);
        btnPlastico = findViewById(R.id.btn_plastico);
        btnmetal = findViewById(R.id.btn_metal);
        btnVidrio = findViewById(R.id.btn_vidrio);
        btnElectro = findViewById(R.id.btn_electronico);
        envio = new Envio(0, 1, 1, "Diego Rodriguez", 0, 0, 0, 0, 0, 0, 0);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.principal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            finish();
            Intent intent = new Intent(Principal.this, Confirmar_Recojo.class);
            startActivity(intent);

        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

            finish();
            Intent intent = new Intent(Principal.this, MainActivity.class);
            startActivity(intent);

        }
        else if(id==R.id.nav_salir)
        {
            finish();
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void mensaje(String msj) {
        Toast.makeText(getApplicationContext(), msj, Toast.LENGTH_SHORT).show();
    }

    public void mensajeDialoj() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Principal.this);

        builder.setTitle("Desea confirmar el recojo de sus residuos?");
        builder.setMessage(" ");

        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


                int total = 0;

                envio.setNombreUsuario("Diego Rodriguez");
                envio.setPtRamking(envio.sumaPuntos());
                envio.setIdEnvio(0);


                enviarDatosRest(envio);

                startActivity(new Intent(Principal.this, Confirmar_Recojo.class));


                dialog.dismiss();
                dialog.cancel();
            }
        });

        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                validabandera=false;

                mensaje("Cancelo el envio");
            }
        });

        AlertDialog dialog = builder.create();

        dialog.show();
    }
}
