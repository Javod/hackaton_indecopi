package com.example.diego.indecopi_reciclaje.conexion.rest;

import com.example.diego.indecopi_reciclaje.beans.Envio;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface RestClient {

    String URL_ENVIAR="envio.guardar";
    String URL_LISTA_MARCADORE="envios.totales";

    @POST(URL_ENVIAR)
    Call<Envio> enviarPedido(@Body Envio envio);

    @GET(URL_LISTA_MARCADORE)
    Call<List<Envio>> getEnvios();

}
