package com.example.diego.indecopi_reciclaje;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

public class Confirmar_Recojo extends AppCompatActivity {

    private Button btnEscanear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmar__recojo);

        btnEscanear = findViewById(R.id.btn_escanear);


        btnEscanear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scanearCodigo();
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        IntentResult result = IntentIntegrator.parseActivityResult(requestCode,resultCode,data);

        if(result!=null)
        {
            if(result.getContents()==null)
            {
                Toast.makeText(Confirmar_Recojo.this,"Cancelaste la lectura de codigo", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(Confirmar_Recojo.this,"Recojo verificado, obtuvo 5 puntos", Toast.LENGTH_LONG).show();
                startActivity(new Intent(Confirmar_Recojo.this, VistaMensajeFinal.class));

            }
        }
        else
        {
            super.onActivityResult(requestCode,resultCode,data);
        }
    }

    public void scanearCodigo()
    {
        IntentIntegrator intent = new IntentIntegrator(this);
        intent.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);


        intent.setPrompt("ESCANEAR CODIGO");
        intent.setCameraId(0);
        intent.setBeepEnabled(false);
        intent.setBarcodeImageEnabled(true);
        intent.setOrientationLocked(true);

        intent.initiateScan();
    }
}
