package com.example.diego.indecopi_reciclaje.beans;

import java.io.Serializable;

public class Envio implements Serializable {

    private int IdEnvio;

    private double latitud;
    private double longitud;
    private String nombreUsuario;
    private int ptPapel;
    private int ptVidrio;
    private int ptMetal;
    private int ptPlastico;
    private int ptElectro;
    private int ptTotal;
    private int ptRamking;


    public Envio()
    {

    }

    public Envio(int idEnvio, double latitud, double longitud, String nombreUsuario, int ptPapel, int ptVidrio, int ptMetal, int ptPlastico, int ptElectro, int ptTotal, int ptRamking) {
        IdEnvio = idEnvio;
        this.latitud = latitud;
        this.longitud = longitud;
        this.nombreUsuario = nombreUsuario;
        this.ptPapel = ptPapel;
        this.ptVidrio = ptVidrio;
        this.ptMetal = ptMetal;
        this.ptPlastico = ptPlastico;
        this.ptElectro = ptElectro;
        this.ptTotal = ptTotal;
        this.ptRamking = ptRamking;
    }

    public int getIdEnvio() {
        return IdEnvio;
    }

    public void setIdEnvio(int idEnvio) {
        IdEnvio = idEnvio;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public int getPtPapel() {
        return ptPapel;
    }

    public void setPtPapel(int ptPapel) {
        this.ptPapel = ptPapel;
    }

    public int getPtVidrio() {
        return ptVidrio;
    }

    public void setPtVidrio(int ptVidrio) {
        this.ptVidrio = ptVidrio;
    }

    public int getPtMetal() {
        return ptMetal;
    }

    public void setPtMetal(int ptMetal) {
        this.ptMetal = ptMetal;
    }

    public int getPtPlastico() {
        return ptPlastico;
    }

    public void setPtPlastico(int ptPlastico) {
        this.ptPlastico = ptPlastico;
    }

    public int getPtElectro() {
        return ptElectro;
    }

    public void setPtElectro(int ptElectro) {
        this.ptElectro = ptElectro;
    }

    public int getPtTotal() {
        return ptTotal;
    }

    public void setPtTotal(int ptTotal) {
        this.ptTotal = ptTotal;
    }

    public int getPtRamking() {
        return ptRamking;
    }

    public void setPtRamking(int ptRamking) {
        this.ptRamking = ptRamking;
    }

    public int sumaPuntos()
    {
        return  ptPapel+ptElectro+ptMetal+ptPlastico+ptVidrio;
    }

    @Override
    public String toString() {
        return "Envio{" +
                "IdEnvio=" + IdEnvio +
                ", latitud=" + latitud +
                ", longitud=" + longitud +
                ", nombreUsuario='" + nombreUsuario + '\'' +
                ", ptPapel=" + ptPapel +
                ", ptVidrio=" + ptVidrio +
                ", ptMetal=" + ptMetal +
                ", ptPlastico=" + ptPlastico +
                ", ptElectro=" + ptElectro +
                ", ptTotal=" + ptTotal +
                ", ptRamking=" + ptRamking +
                '}';
    }
}
