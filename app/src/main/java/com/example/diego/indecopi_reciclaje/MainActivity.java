package com.example.diego.indecopi_reciclaje;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView txtIngresar;
    private Button btnIngresar;
    private EditText txtDni;
    private EditText txtPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        txtIngresar=findViewById(R.id.txt_registrarse);
        btnIngresar= findViewById(R.id.btn_ingresar_ventana);
        txtDni = findViewById(R.id.txt_dni);
        txtPass = findViewById(R.id.txt_pass);

        txtIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, Login.class);

                startActivity(intent);
            }
        });

        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(txtDni.getText().toString().equals("admin") && txtPass.getText().toString().equals("admin"))
                {
                    Intent intent = new Intent(MainActivity.this, MapsActivity.class);

                    startActivity(intent);
                }
                else {
                    Intent intent = new Intent(MainActivity.this, Principal.class);

                    startActivity(intent);
                }



            }
        });

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
